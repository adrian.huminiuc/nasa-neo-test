<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Asteroid;

/**
 * AsteroidRepository
 *
 */
class AsteroidRepository extends \Doctrine\ORM\EntityRepository implements AsteroidRepositoryInterface
{
    /**
     * @param Asteroid $asteroid
     * @return Asteroid
     */
    public function create(Asteroid $asteroid)
    {
        $this->_em->persist($asteroid);
        $this->_em->flush();

        return $asteroid;
    }


    /**
     * @param $asteroids
     * @param bool|false $checkForDuplicates
     * @param int $batchSize
     * @return int
     */
    public function batchInsert($asteroids, $checkForDuplicates = false, $batchSize = 20)
    {
        $persistedAsteroids = 0;
        for ($index = 0; $index < count($asteroids); $index++) {

            $asteroid = $asteroids[$index];
            if (!$asteroid instanceof Asteroid) {
                throw new \LogicException('Cannot persist non asteroid entity as asteroid');
            }

            /**
             * Check for duplicates
             * Slows down the insert significantly
             *
             * @todo
             * Refactor with custom query into insert ignore or upsert
             */
            if ($checkForDuplicates) {
                $duplicate = $this->findOneBy(['neoReferenceId' => $asteroid->getNeoReferenceId()]);

                if ($duplicate instanceof Asteroid) {
                    continue;
                }
            }

            $this->_em->persist($asteroid);
            $persistedAsteroids++;

            if ($index % $batchSize === 0 && $index > 0) {
                $this->_em->flush();
                $this->_em->clear();
            }
        }

        $this->_em->flush();
        $this->_em->clear();

        return $persistedAsteroids;
    }

}
