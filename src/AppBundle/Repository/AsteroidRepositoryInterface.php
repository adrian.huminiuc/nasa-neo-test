<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Asteroid;

interface AsteroidRepositoryInterface
{
    /**
     * Finds entities by a set of criteria.
     *
     * @param array      $criteria
     * @param array|null $orderBy
     * @param int|null   $limit
     * @param int|null   $offset
     *
     * @return array The objects.
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);
    /**
     * Finds a single entity by a set of criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function findOneBy(array $criteria, array $orderBy = null);
    /**
     * @param Asteroid $asteroid
     * @return Asteroid
     */
    public function create(Asteroid $asteroid);

    /**
     * @param $asteroids
     * @param bool|false $checkForDuplicates
     * @param int $batchSize
     * @return int
     */
    public function batchInsert($asteroids, $checkForDuplicates = false, $batchSize = 20);
}