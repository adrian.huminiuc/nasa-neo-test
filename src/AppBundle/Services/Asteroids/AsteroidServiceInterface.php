<?php declare(strict_types = 1);


namespace AppBundle\Services\Asteroids;


interface AsteroidServiceInterface
{
    /**
     * @param array $criteria
     * @param float $page
     * @param float $limit
     * @param $order
     * @return array
     */
    public function listAsteroids(array $criteria, float $page, float $limit, array $order = []) : array;
}