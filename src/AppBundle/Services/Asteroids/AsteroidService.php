<?php declare(strict_types = 1);

namespace AppBundle\Services\Asteroids;


use AppBundle\Repository\AsteroidRepositoryInterface;

/**
 * Class AsteroidService
 * @package AppBundle\Services\Asteroids
 */
class AsteroidService implements AsteroidServiceInterface
{

    public function __construct(AsteroidRepositoryInterface $repository)
    {
        $this->asteroidRepo = $repository;
    }

    /**
     * @param array $criteria
     * @param float $page
     * @param float $limit
     * @param $order
     * @return array
     */
    public function listAsteroids(array $criteria, float $page, float $limit, array $order = []) : array
    {
        $offset = ($page - 1) * $limit;
        $order = empty($order) ? ['id' => 'ASC'] : $order;

        return $this->asteroidRepo->findBy($criteria, $order, $limit, $offset);
    }
}