<?php

namespace AppBundle\Services\Asteroids;

use GuzzleHttp\ClientInterface;

class NasaAsteroidClient
{

    private $httpClient;
    private $url;
    private $apiKey;

    public function __construct(ClientInterface $httpClient, $url, $apiKey)
    {
        $this->httpClient = $httpClient;
        $this->url = $url;
        $this->apiKey = $apiKey;
    }

    /**
     * Fetch the nasa feed from today:
     * @link https://api.nasa.gov/api.html#neows-swagger
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getFeedToday()
    {
        $todayFeedUrl = sprintf(
            $this->url.'feed/today?detailed=true&api_key=%s',
            $this->apiKey
        );

        return $this->httpClient->request('GET', $todayFeedUrl);
    }


    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getFeed(\DateTime $startDate, \DateTime $endDate)
    {
        $startDay = $startDate->format('Y-m-d');
        $endDay = $endDate->format('Y-m-d');


        $todayFeedUrl = sprintf(
            $this->url.'feed?start_date=%s&end_date=%s&detailed=true&api_key=%s',
            $startDay,
            $endDay,
            $this->apiKey
        );

        return $this->httpClient->request('GET', $todayFeedUrl);
    }
}