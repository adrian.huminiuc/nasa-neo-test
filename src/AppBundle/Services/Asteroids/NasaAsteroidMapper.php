<?php

namespace AppBundle\Services\Asteroids;


use AppBundle\Entity\Asteroid;

class NasaAsteroidMapper
{

    /**
     * @param $rawAsteroid
     * @return Asteroid
     */
    public function getAsteroidFromRawNasaAsteroid($rawAsteroid)
    {
        $asteroid = new Asteroid();
        $asteroid->setName($rawAsteroid['name']);
        $asteroid->setNeoReferenceId($rawAsteroid['neo_reference_id']);
        $asteroid->setIsPotentiallyHazardous($rawAsteroid['is_potentially_hazardous_asteroid']);
        $asteroid->setCloseApproachDate(
            \DateTime::createFromFormat('Y-m-d', $rawAsteroid['close_approach_data'][0]['close_approach_date'])
        );
        $asteroid->setMissDistance((float)$rawAsteroid['close_approach_data'][0]['miss_distance']['kilometers']);
        $asteroid->setOrbitingBody($rawAsteroid['close_approach_data'][0]['orbiting_body']);
        $asteroid->setRelativeVelocity(
            (float)$rawAsteroid['close_approach_data'][0]['relative_velocity']['kilometers_per_hour']
        );

        $asteroid->setEstimatedDiameter(
            (0 + $rawAsteroid['estimated_diameter']['meters']['estimated_diameter_min'] +
                $rawAsteroid['estimated_diameter']['meters']['estimated_diameter_max']) / 2
        );

        return $asteroid;
    }

}