<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class AsteroidController extends Controller
{
    /**
     * @Route("/asteroids")
     * @Method({"GET"})
     *
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listAsteroidsAction(Request $request)
    {
        $page = $request->get('page') ?? 1;
        $limit = $request->get('limit') ?? 20;


        $asteroids = $this->get('asteroid.service')->listAsteroids(
            [],
            $page,
            $limit
        );

        return new JsonResponse($asteroids);
    }

    /**
     * @Route("/asteroids/dangerous")
     * @Method({"GET"})
     *
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listDangerousAsteroidsAction(Request $request)
    {
        $page = $request->get('page') ?? 1;
        $limit = $request->get('limit') ?? 20;


        $asteroids = $this->get('asteroid.service')->listAsteroids(
            ['isPotentiallyHazardous' => true],
            $page,
            $limit
        );

        return new JsonResponse($asteroids);
    }
}