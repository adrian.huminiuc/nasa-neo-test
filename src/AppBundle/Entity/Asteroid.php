<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * "neo_reference_id": "2004581",
 * "name": "4581 Asclepius (1989 FC)",
 * "nasa_jpl_url": "http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=2004581",
 * "absolute_magnitude_h": 20.7,
 * "estimated_diameter": {
 *
 * "meters": {
 * "estimated_diameter_min": 192.5550781879,
 * "estimated_diameter_max": 430.566244241
 * }
 *
 * },
 * "is_potentially_hazardous_asteroid": true,
 * "close_approach_data": [
 * {
 * "close_approach_date": "2017-05-17",
 * "epoch_date_close_approach": 1495004400000,
 * "relative_velocity": {
 * "kilometers_per_second": "8.8435099544",
 *
 * },
 * "miss_distance": {
 * "kilometers": "54514200",
 *
 * },
 * "orbiting_body": "Earth"
 *
 *
 * Asteroid
 *
 * @ORM\Table(name="asteroid")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AsteroidRepository")
 */
class Asteroid implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="neoReferenceId", type="string", length=255, unique=true)
     */
    private $neoReferenceId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="estimatedDiameter", type="decimal", precision=10, scale=2)
     */
    private $estimatedDiameter;

    /**
     * @var bool
     *
     * @ORM\Column(name="isPotentiallyHazardous", type="boolean")
     */
    private $isPotentiallyHazardous;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closeApproachDate", type="datetime")
     */
    private $closeApproachDate;

    /**
     * @var string
     *
     * @ORM\Column(name="relativeVelocity", type="decimal", precision=10, scale=2)
     */
    private $relativeVelocity;

    /**
     * @var string
     *
     * @ORM\Column(name="missDistance", type="decimal", precision=10, scale=2)
     */
    private $missDistance;

    /**
     * @var string
     *
     * @ORM\Column(name="orbitingBody", type="string", length=255)
     */
    private $orbitingBody;

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'orbitingBody' => $this->getOrbitingBody(),
            'missDistance' => $this->getMissDistance(),
            'relativeVelocity' => $this->getRelativeVelocity(),
            'closeApproachDate' => $this->getCloseApproachDate(),
            'isPotentiallyHazardous' => $this->getIsPotentiallyHazardous(),
            'estimatedDiameter' => $this->getEstimatedDiameter(),
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set neoReferenceId
     *
     * @param string $neoReferenceId
     *
     * @return Asteroid
     */
    public function setNeoReferenceId($neoReferenceId)
    {
        $this->neoReferenceId = $neoReferenceId;

        return $this;
    }

    /**
     * Get neoReferenceId
     *
     * @return string
     */
    public function getNeoReferenceId()
    {
        return $this->neoReferenceId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Asteroid
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set estimatedDiameter
     *
     * @param string $estimatedDiameter
     *
     * @return Asteroid
     */
    public function setEstimatedDiameter($estimatedDiameter)
    {
        $this->estimatedDiameter = $estimatedDiameter;

        return $this;
    }

    /**
     * Get estimatedDiameter
     *
     * @return string
     */
    public function getEstimatedDiameter()
    {
        return $this->estimatedDiameter;
    }

    /**
     * Set isPotentiallyHazardous
     *
     * @param boolean $isPotentiallyHazardous
     *
     * @return Asteroid
     */
    public function setIsPotentiallyHazardous($isPotentiallyHazardous)
    {
        $this->isPotentiallyHazardous = $isPotentiallyHazardous;

        return $this;
    }

    /**
     * Get isPotentiallyHazardous
     *
     * @return bool
     */
    public function getIsPotentiallyHazardous()
    {
        return $this->isPotentiallyHazardous;
    }

    /**
     * Set closeApproachDate
     *
     * @param \DateTime $closeApproachDate
     *
     * @return Asteroid
     */
    public function setCloseApproachDate($closeApproachDate)
    {
        $this->closeApproachDate = $closeApproachDate;

        return $this;
    }

    /**
     * Get closeApproachDate
     *
     * @return \DateTime
     */
    public function getCloseApproachDate()
    {
        return $this->closeApproachDate;
    }

    /**
     * Set relativeVelocity
     *
     * @param string $relativeVelocity
     *
     * @return Asteroid
     */
    public function setRelativeVelocity($relativeVelocity)
    {
        $this->relativeVelocity = $relativeVelocity;

        return $this;
    }

    /**
     * Get relativeVelocity
     *
     * @return string
     */
    public function getRelativeVelocity()
    {
        return $this->relativeVelocity;
    }

    /**
     * Set missDistance
     *
     * @param string $missDistance
     *
     * @return Asteroid
     */
    public function setMissDistance($missDistance)
    {
        $this->missDistance = $missDistance;

        return $this;
    }

    /**
     * Get missDistance
     *
     * @return string
     */
    public function getMissDistance()
    {
        return $this->missDistance;
    }

    /**
     * Set orbitingBody
     *
     * @param string $orbitingBody
     *
     * @return Asteroid
     */
    public function setOrbitingBody($orbitingBody)
    {
        $this->orbitingBody = $orbitingBody;

        return $this;
    }

    /**
     * Get orbitingBody
     *
     * @return string
     */
    public function getOrbitingBody()
    {
        return $this->orbitingBody;
    }
}

