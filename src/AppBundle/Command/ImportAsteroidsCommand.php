<?php

namespace AppBundle\Command;


use AppBundle\Entity\Asteroid;
use AppBundle\Repository\AsteroidRepository;
use AppBundle\Services\Asteroids\NasaAsteroidClient;
use AppBundle\Services\Asteroids\NasaAsteroidMapper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportAsteroidsCommand extends ContainerAwareCommand
{
    /**
     * @var NasaAsteroidMapper
     */
    private $nasaMapper;
    /**
     * @var NasaAsteroidClient
     */
    private $nasaClient;
    /**
     * @var AsteroidRepository
     */
    private $asteroidRepo;

    /**
     * Configures the current command.
     */
    public function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('nasa:lookup-asteroids')
            ->addArgument('start', InputArgument::REQUIRED, 'The start date.')
            ->addArgument('end', InputArgument::REQUIRED, 'The end date. No more than 7.')
            ->setDescription('Imports asteroids based on the start and end arguments.')
            ->setHelp('Imports asteroids based on the start and end arguments.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     * @return OutputInterface
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->nasaMapper = $this->getContainer()->get('nasa.mapper');
        $this->nasaClient = $this->getContainer()->get('nasa.client');
        $this->asteroidRepo = $this->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Asteroid');


        $startDate = \DateTime::createFromFormat('Y-m-d', $input->getArgument('start'));
        $endDate = \DateTime::createFromFormat('Y-m-d', $input->getArgument('end'));

        /**
         * Simple validation for start and end dates
         */
        if (false === $startDate) {
            throw new \InvalidArgumentException('Start date not a valid YYYY-MM-DD string');
        }

        if (false === $endDate) {
            throw new \InvalidArgumentException('Start date not a valid YYYY-MM-DD string');
        }

        $sevenDaysPastStartDate = clone $startDate;
        if ($startDate > $endDate || $sevenDaysPastStartDate->modify('+7 days') < $endDate) {
            throw new \InvalidArgumentException('Invalid end date range');
        }


        $response = $this->nasaClient->getFeed($startDate, $endDate);
        if ($response->getStatusCode() !== 200) {
            throw new \Exception($response->getReasonPhrase());
        }

        $response = json_decode((string)$response->getBody(), true);

        /**
         * Itterate the possible near earth asteroids, and prepare for persistance
         */

        $asteroidsToPersist = [];
        foreach ($response['near_earth_objects'] as $asteroidsPerDay) {
            foreach ($asteroidsPerDay as $rawAsteroid) {
                $asteroidsToPersist[] = $this->nasaMapper->getAsteroidFromRawNasaAsteroid($rawAsteroid);
            }
        }

        $persistedAsteroids = $this->asteroidRepo->batchInsert($asteroidsToPersist, true);

        $output->writeln('<info>Successfully persisted '.$persistedAsteroids.' asteroids!</info>');
        return $output;
    }

}