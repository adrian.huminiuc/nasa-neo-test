<?php

namespace Tests\AppBundle\Services\Asteroids;


use AppBundle\Repository\AsteroidRepository;
use AppBundle\Repository\AsteroidRepositoryInterface;
use AppBundle\Services\Asteroids\AsteroidService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class AsteroidServiceTest extends KernelTestCase
{

    public function testAsteroidServiceHandlesParams()
    {
        $asteroidRepoMock = $this->createMock(AsteroidRepositoryInterface::class);

        $asteroidRepoMock->expects($this->once())
            ->method('findBy')
            ->willReturn([])
            //limit has to be 10 and offeset by 20
            ->with([], ['id' => 'ASC'], 10, 20);

        $asteroidService = new AsteroidService($asteroidRepoMock);
        $asteroids = $asteroidService->listAsteroids([], 3, 10);

        $this->assertEquals($asteroids, []);
    }

}