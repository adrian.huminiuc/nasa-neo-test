<?php

namespace tests\AppBundle\Services\Asteroids;


use AppBundle\Services\Asteroids\NasaAsteroidClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class NasaAsteroidClientTest extends KernelTestCase
{


    public function testFeedUrl()
    {
        $url = 'https://test.de/';
        $key = 'testKEY';
        $dateString = '2017-01-01';

        $expectedUrlCalled = sprintf(
            '%sfeed?start_date=%s&end_date=%s&detailed=true&api_key=%s',
            $url,
            $dateString,
            $dateString,
            $key
        );


        $httpClient = $this->createMock(ClientInterface::class);
        $httpClient->expects($this->once())
            ->method('request')
            ->willReturn(new Response())
            ->with('GET', $expectedUrlCalled);



        $nasaClient = new NasaAsteroidClient($httpClient, $url, $key );
        $nasaClient->getFeed(new \DateTime('2017-01-01'), new \DateTime('2017-01-01'));

    }
}